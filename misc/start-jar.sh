#!/bin/bash
PID=`ps -ef | grep echo-sshd-server.jar | grep -v grep | awk '{print $2}'`
if [ -z "$PID" ]
then
  nohup authbind /opt/graalvm-jdk-17/bin/java -Dcom.sun.management.jmxremote -Djava.rmi.server.hostname=127.0.0.1 -Dcom.sun.management.jmxremote.port=2020 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -jar echo-sshd-server.jar conf > logs/exec.log 2>&1 &
  sleep 1 
  PID=`ps -ef | grep echo-sshd-server.jar | grep -v grep | awk '{print $2}'`
  echo "started echo-sshd-server.jar pid: $PID"
else
  echo "already exist echo-sshd-server.jar pid: $PID"
fi
