## Introduction

This is a dummy SSH server created using MINA SSHD Library and Springboot 3.
The point is for me to learn all SSH-hacker-bot-command patterns around the world.
I share this project so that others can learn this as well.

## How to build

```
mvn clean package

```

## Auth Bind Setup

```
sudo touch /etc/authbind/byport/22
sudo chmod 777 /etc/authbind/byport/22
```

## Expected OS
Linux

##Expected Directory Structory

```
$ tree echo-ssh-server/
echo-ssh-server/
├── conf
│   ├── hash-replies.properties
│   ├── log4j2.xml
│   ├── private.key
│   ├── regex-mapping.properties
│   └── springboot.yml
├── data
│   ├── remote-ip-info-db.mv.db
│   └── remote-ip-info-db.trace.db
├── echo-sshd-server-1.4.1.jar
├── echo-sshd-server.jar -> echo-sshd-server-1.4.1.jar
├── logs
├── start-jar.sh
└── stop.sh

```

## How to start
```
./start-jar.sh
```
## How to stop
```
./stop.sh
```