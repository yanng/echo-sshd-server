package com.example.sshd.config;

import java.io.IOException;
import java.time.Duration;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.hc.client5.http.impl.async.CloseableHttpAsyncClient;
import org.apache.hc.client5.http.impl.async.HttpAsyncClients;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.reactor.IOReactorConfig;
import org.apache.sshd.common.Session;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.Scope;
import org.xmpp.packet.Message;

import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;

@Configuration
@ImportResource("file:conf/scheduler.xml")
public class AppConfig {

    @Value("${xmpp-component.config-json}")
    private String xmppComponentConfigJson;

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
    public XmppComponentConfig xmppComponentConfig() throws StreamReadException, DatabindException, IOException {
	return new ObjectMapper().readValue(new java.io.File(xmppComponentConfigJson), XmppComponentConfig.class);
    }

    @Bean("userAdminCache")
    public Cache<String, Message> userAdminCache() {
	return Caffeine.newBuilder().expireAfterWrite(Duration.ofMinutes(1)).build();
    }

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
    public Map<String, Session> remoteSessionMapping() {
	return new ConcurrentHashMap<>();
    }

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
    public Map<String, String> ipInfoMapping() {
	return new ConcurrentHashMap<>();
    }

    @Bean
    public CloseableHttpAsyncClient asyncClient() {
	final IOReactorConfig ioReactorConfig = IOReactorConfig.custom().build();
	final CloseableHttpAsyncClient client = HttpAsyncClients.custom().setIOReactorConfig(ioReactorConfig).build();
	client.start();
	return client;
    }

    @Bean
    public CloseableHttpClient httpClient() {
	return HttpClients.createDefault();
    }
}
