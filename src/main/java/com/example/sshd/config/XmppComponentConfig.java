package com.example.sshd.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class XmppComponentConfig {

    private static final Logger logger = LoggerFactory.getLogger(XmppComponentConfig.class);

    private String subdomainPrefix;
    private String domain;
    private String host;
    private int port;
    private String secretKey;
    private boolean startEncrypted;

    public String getSubdomainPrefix() {
	return subdomainPrefix;
    }

    public void setSubdomainPrefix(String subdomainPrefix) {
	this.subdomainPrefix = subdomainPrefix;
    }

    public String getDomain() {
	return domain;
    }

    public void setDomain(String domain) {
	this.domain = domain;
    }

    public String getHost() {
	return host;
    }

    public void setHost(String host) {
	this.host = host;
    }

    public int getPort() {
	return port;
    }

    public void setPort(int port) {
	this.port = port;
    }

    public String getSecretKey() {
	return secretKey;
    }

    public void setSecretKey(String secretKey) {
	this.secretKey = secretKey;
    }

    public boolean isStartEncrypted() {
	return startEncrypted;
    }

    public void setStartEncrypted(boolean startEncrypted) {
	this.startEncrypted = startEncrypted;
    }

    @Override
    @JsonIgnore
    public String toString() {
	try {
	    return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this);
	} catch (JsonProcessingException e) {
	    logger.error("convert to json error!", e);
	}
	return "{}";
    }
}
