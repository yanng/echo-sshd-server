package com.example.sshd.core;

import java.net.InetSocketAddress;
import java.util.List;
import java.util.Map;

import org.apache.sshd.common.Session;
import org.apache.sshd.common.SessionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.sshd.service.GeoIpLocator;
import com.example.sshd.service.JdbcService;

@Component
public class EchoSessionListener implements SessionListener {

    private static final Logger logger = LoggerFactory.getLogger(EchoSessionListener.class);
    private static final Logger ipInfoLogger = LoggerFactory.getLogger("ip_info");

    @Autowired
    Map<String, Session> remoteSessionMapping;

    @Autowired
    Map<String, String> ipInfoMapping;

    @Autowired
    GeoIpLocator geoIpLocator;

    @Autowired
    JdbcService jdbcService;

    @Override
    public void sessionCreated(Session session) {
	logger.info("sessionCreated: {}", session);
	if (session.getIoSession().getRemoteAddress() instanceof InetSocketAddress) {
	    InetSocketAddress remoteAddress = (InetSocketAddress) session.getIoSession().getRemoteAddress();
	    String remoteIpAddress = remoteAddress.getAddress().getHostAddress();
	    if (remoteSessionMapping.containsKey(remoteIpAddress)) {
		logger.info("kill old session: {} -> {}", remoteIpAddress, remoteSessionMapping.get(remoteIpAddress));
		remoteSessionMapping.get(remoteIpAddress).close(false);
	    }
	    logger.info("new session: {} -> {}", remoteIpAddress, session);
	    remoteSessionMapping.put(remoteIpAddress, session);
	    if (!ipInfoMapping.containsKey(remoteIpAddress)) {
		List<Map<String, Object>> ipInfoList = jdbcService.getAllRemoteIpInfo(remoteIpAddress);
		if (!ipInfoList.isEmpty()) {
		    ipInfoMapping.put(remoteIpAddress, (String) ipInfoList.get(0).get("remote_ip_info"));
		}
	    }
	}
    }

    @Override
    public void sessionEvent(Session session, Event event) {
	logger.info("sessionEvent: {}, event: {}", session, event);
	if (session.getIoSession().getRemoteAddress() instanceof InetSocketAddress && event == Event.KexCompleted) {
	    InetSocketAddress remoteAddress = (InetSocketAddress) session.getIoSession().getRemoteAddress();
	    String remoteIpAddress = remoteAddress.getAddress().getHostAddress();
	    if (!ipInfoMapping.containsKey(remoteIpAddress)) {
		geoIpLocator.asyncUpdateIpLocationInfo(remoteIpAddress);
	    } else {
		ipInfoLogger.debug("[{}] {}", remoteIpAddress, ipInfoMapping.get(remoteIpAddress));
	    }
	}
    }

    @Override
    public void sessionClosed(Session session) {
	logger.info("sessionClosed: {}", session);
	if (session.getIoSession().getRemoteAddress() instanceof InetSocketAddress) {
	    InetSocketAddress remoteAddress = (InetSocketAddress) session.getIoSession().getRemoteAddress();
	    String remoteIpAddress = remoteAddress.getAddress().getHostAddress();
	    logger.info("removing session: {} -> {}", remoteIpAddress, remoteSessionMapping.get(remoteIpAddress));
	    remoteSessionMapping.remove(remoteIpAddress);
	    ipInfoMapping.remove(remoteIpAddress);
	}
    }

}
