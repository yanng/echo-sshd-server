package com.example.sshd.service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import org.apache.hc.client5.http.async.methods.SimpleHttpRequest;
import org.apache.hc.client5.http.async.methods.SimpleHttpResponse;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.impl.async.CloseableHttpAsyncClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.core5.concurrent.FutureCallback;
import org.apache.hc.core5.http.ClassicHttpResponse;
import org.apache.hc.core5.http.HttpException;
import org.apache.hc.core5.http.io.HttpClientResponseHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class GeoIpLocator {

    private static final Logger logger = LoggerFactory.getLogger(GeoIpLocator.class);
    private static final Logger ipInfoLogger = LoggerFactory.getLogger("ip_info");

    @Autowired
    Map<String, String> ipInfoMapping;

    @Autowired
    JdbcService jdbcService;

    @Autowired
    CloseableHttpAsyncClient asyncClient;

    @Autowired
    CloseableHttpClient httpClient;

    @Value("${ssh-server.ip-info-api.url:http://ip-api.com/json/%s}")
    private String ipInfoApiUrl;

    @Value("${ssh-server.ip-info-api.method:GET}")
    private String ipInfoApiMethod;

    public String getIpLocationInfo(String remoteIpAddress) {
	HttpGet httpGet = new HttpGet(String.format(ipInfoApiUrl, remoteIpAddress));
	try {
	    return httpClient.execute(httpGet, new HttpClientResponseHandler<String>() {

		@Override
		public String handleResponse(ClassicHttpResponse result) throws HttpException, IOException {
		    String body = new String(result.getEntity().getContent().readAllBytes(), StandardCharsets.UTF_8);
		    logger.info("[{}] httpClient.execute completed, response code: {}, body: {}", remoteIpAddress,
			    result.getCode(), body);
		    ipInfoMapping.put(remoteIpAddress, body);
		    int inserted = jdbcService.insertRemoteIpInfo(remoteIpAddress, body);
		    ipInfoLogger.info("[{}] {}, inserted = {}", remoteIpAddress, ipInfoMapping.get(remoteIpAddress),
			    inserted);
		    return body;
		}

	    });
	} catch (IOException e) {
	    logger.error("[getIpLocationInfo] IO Exception has occurred!", e);
	    return null;
	}
    }

    public void asyncUpdateIpLocationInfo(String remoteIpAddress) {
	asyncClient.execute(SimpleHttpRequest.create(ipInfoApiMethod, String.format(ipInfoApiUrl, remoteIpAddress)),
		new FutureCallback<SimpleHttpResponse>() {

		    @Override
		    public void completed(SimpleHttpResponse result) {
			logger.info("[{}] asyncClient.execute completed, result: {}, content-type: {}, body: {}",
				remoteIpAddress, result, result.getContentType(), result.getBodyText());
			ipInfoMapping.put(remoteIpAddress, result.getBodyText());
			int inserted = jdbcService.insertRemoteIpInfo(remoteIpAddress, result.getBodyText());
			ipInfoLogger.info("[{}] {}, inserted = {}", remoteIpAddress, ipInfoMapping.get(remoteIpAddress),
				inserted);
		    }

		    @Override
		    public void failed(Exception exception) {
			logger.info("[{}] asyncClient.execute failed, exception: {}", remoteIpAddress, exception);
		    }

		    @Override
		    public void cancelled() {
			logger.info("[{}] asyncClient.execute cancelled.", remoteIpAddress);
		    }
		});
    }
}
