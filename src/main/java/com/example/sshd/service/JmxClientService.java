package com.example.sshd.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.management.Attribute;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanInfo;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.openmbean.CompositeData;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class JmxClientService {

    private static final Logger logger = LoggerFactory.getLogger(ReplyService.class);

    public String process(String[] args) {
	// Example
	// 1st parameter: service:jmx:rmi:///jndi/rmi://127.0.0.1:2020/jmxrmi
	// 2nd parameter: java.lang:type=Memory

	StringBuilder output = new StringBuilder();
	try {
	    if (args.length > 2) {
		Runtime.getRuntime().freeMemory();
		JMXServiceURL url = new JMXServiceURL(args[1]);
		JMXConnector jmxc = JMXConnectorFactory.connect(url, null);
		MBeanServerConnection mbsc = jmxc.getMBeanServerConnection();

		ObjectName mbeanName = new ObjectName(args[2]);

		MBeanInfo info = mbsc.getMBeanInfo(mbeanName);
		MBeanAttributeInfo[] attribute = info.getAttributes();

		logger.info("[process] args.length: {}", args.length);
		if (args.length > 3) {
		    for (MBeanAttributeInfo attr : attribute) {
			List<Attribute> alist = mbsc.getAttributes(mbeanName, new String[] { attr.getName() }).asList();
			if (args[3].equals(attr.getName())) {
			    Optional<Map<String, Object>> opt = alist.stream().filter(a -> a.getName().equals(args[3]))
				    .map(a -> toMap((CompositeData) a.getValue())).findFirst();
			    if (opt.isPresent()) {
				output.append(attr.getName() + ": " + opt.get() + "\r\n");
			    }
			}
		    }
		} else {
		    for (MBeanAttributeInfo attr : attribute) {
			List<Attribute> alist = mbsc.getAttributes(mbeanName, new String[] { attr.getName() }).asList();
			output.append(attr.getName() + ": " + alist + "\r\n");
		    }
		}
		jmxc.close();
	    } else {
		output.append(
			"Example: jmx_client service:jmx:rmi:///jndi/rmi://127.0.0.1:2020/jmxrmi java.lang:type=Memory HeapMemoryUsage\r\n");
	    }
	} catch (Exception e) {
	    logger.error("process cmd failed: {}", Arrays.asList(args), e);
	}
	return output.toString();
    }

    public static Map<String, Object> toMap(CompositeData cd) {
	if (cd == null)
	    throw new IllegalArgumentException("composite data should not be null");
	Map<String, Object> map = new HashMap<String, Object>();
	Set<String> itemNames = cd.getCompositeType().keySet();
	for (String itemName : itemNames) {
	    Object item = cd.get(itemName);
	    map.put(itemName, item);
	}
	return map;
    }
}
