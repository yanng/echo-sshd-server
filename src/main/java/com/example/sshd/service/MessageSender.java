package com.example.sshd.service;

import org.springframework.beans.factory.annotation.Autowired;

public class MessageSender {

    @Autowired
    EchoComponent echoComponent;
    
    private String fromJID, toJID, message;
    
    public void sendMessage() {
	echoComponent.sendMessage(fromJID, toJID, message);
    }

    public String getFromJID() {
        return fromJID;
    }

    public void setFromJID(String fromJID) {
        this.fromJID = fromJID;
    }

    public String getToJID() {
        return toJID;
    }

    public void setToJID(String toJID) {
        this.toJID = toJID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
